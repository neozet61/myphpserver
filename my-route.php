<?php

$uri = $_SERVER['REQUEST_URI'];

if ($uri == "/") {
    include "./web/index.php";
} elseif (str_starts_with($uri, "/users/") and is_numeric(substr($uri, 7))) {
    $id = substr($uri, 7);
    include "./web/controllerUserCard.php";
}elseif (str_starts_with($uri, "/api/users/") and is_numeric(substr($uri, 11))){
    $id = substr($uri, 11);
    include "./api/apiInfoUser.php";
} elseif (str_starts_with($uri, "/api/users")){
    include "./api/apiUsers.php";
} else {
    if (str_starts_with($uri, "/api/")){
        include "./api/api404.php";
    }else{include "./web/code404.php";}
}