<?php
include "controllerCSV.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Users list</title>
</head>
<body>
  <h1>List of Users</h1>
  <ul>
    <?php
        foreach($persons as $person)
            echo "<li><a href='/users/" . $person[0] . "'>" . $person[1] . " ". $person[2] . "</a></li>";
    ?>
  </ul>
</body>