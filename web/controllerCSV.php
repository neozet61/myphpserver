<?php

// Парсим данные из файла csv
$fileContent = file_get_contents('data.csv');
$rows = explode("\n", $fileContent);

$headings = $rows[0];
$headingsBase = explode(";", $headings);

array_shift($rows);

$persons = [];

foreach ($rows as $row) {

    $fields = explode(";", $row);

    if (count($fields) != 4) {
        continue;
    }

    $persons[] = [
        ($fields[0])*1,
        $fields[1],
        $fields[2],
        $fields[3],
    ];


}

