<?php
include "controllerCSV.php";

$placeInArray = ($id * 1) - 1;

if (!array_key_exists($placeInArray, $persons)) {
    header("Location: ./web/code404.php");
}

$person = $persons[$placeInArray];

include "userCard.php";