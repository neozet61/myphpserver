<?php
include "./web/controllerCSV.php";

$placeInArray = (int)$id - 1;

if (!array_key_exists($placeInArray, $persons)) {
    http_response_code(404);
    header('Content-Type: application/json');
    echo json_encode(["error"=> "User $id is not found."]);
    die();
}

$person = $persons[$placeInArray];

$result = [
    "id" => ($person[0] * 1),
    "first_name" => $person[1],
    "second_name" => $person[2],
    "e-mail" => $person[3]
];

header('Content-Type: application/json');

echo json_encode($result);
